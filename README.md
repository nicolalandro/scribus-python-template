# Scribus Python Template
In this repo we will see how to fill a Scribus template .sla file and create a new .sla with correct values.

## Run with docekr
* build
```
docker-compose build
```
* Fill template
```
docker-compose run python
```
* Export to pdf into docker
```
docker-compose run scribus -g -py scripts/to-pdf.py -- templated.sla 
```

## Run without docekr
* run fill template
```
# python3.10
pip install pyscribus
python scripts/pyscribus_templateing.py

# it will create a templated.sla file
```
* convert to pdf
```
# install scribus 1.6.1
scribus -g -py scripts/to-pdf.py -- templated.sla 
```

## References
* Python: programming language used
* Docker, Docker Compose: to work on any machine
* [Scribus](https://www.scribus.net/): software for publishing (Like InDesign)
* pyscribus:
  * [doc](https://etnadji.fr/pyscribus/guide.html)
  * [code](https://framagit.org/etnadji/pyscribus)
* Use scribus with python:
  * [commandline script](https://wiki.scribus.net/canvas/Command_line_scripts)
