# Produces a PDF for the SLA passed as a parameter.
# Uses the same file name and replaces the .sla extension with .pdf
#
# usage:
# scribus -g -py to-pdf.py file.sla

import os
import sys

###################################
# 
#  check scribus
#
###################################

try:
    import scribus
except ImportError:
    # if the script does not run inside of scribus, launch scribus with this script as paremeter
    
    filename = []

    if (len(sys.argv) == 2) :
        print("launching the script for " + sys.argv[1])
        filename.append(sys.argv[1])
    else:
        print("launching the script for each .sla in the directory.")
        for file in os.listdir('.'):
            filenameSplit = os.path.splitext(file)
            if filenameSplit[-1] == '.sla' and filenameSplit[0].find('_autosave_') == -1:
                filename.append(file)

    for file in filename :
        print(file)
        os.system('scribus -g -py ' + sys.argv[0] + ' -- ' + file)
    
    sys.exit(1)

###################################
# 
#  Export PDF
#
###################################

if scribus.haveDoc() :
    filename = os.path.splitext(scribus.getDocName())[0]
    pdf = scribus.PDFfile()
    pdf.file = filename+".pdf"
    pdf.save()
else :
    print("No file open")
