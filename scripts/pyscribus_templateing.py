# https://etnadji.fr/pyscribus/guide/en/templating.html
import pyscribus.file as pyf

# intro.sla is parsed at instanciation
parsed = pyf.ScribusFile("1.5.5", "template.sla", templating=True)
stories = parsed.templatable_stories

datas = [
    {
        "%Title%": "My title",
        "%Text%": "Lorem ipsum",
        "%Caption%": "Caption",
        "%Footer%": "Footer"
    }
]

# For each stories, we replace/feed the placeholders with their contents

for index, templatable_story in enumerate(stories):
    print(index, templatable_story)
    parsed.stories.feed(templatable_story, datas[0])

# Change image
image_frames = parsed.pageobjects.filter(object_type="image")
imgbox = image_frames[0]
xml = imgbox.toxml()
xml.attrib["PFILE"] = './fake.jpeg'
imgbox.fromxml(xml)

# Saving the templated document as a new one
parsed.save("templated.sla")